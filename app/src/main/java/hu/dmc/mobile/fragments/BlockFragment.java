package hu.dmc.mobile.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import dmc.org.mobile.R;
import hu.dmc.mobile.control.json.BlockUtil;
import hu.dmc.mobile.model.DataContext;
import hu.dmc.mobile.model.SavedTemplate;
import hu.dmc.mobile.model.Template;
import hu.dmc.mobile.util.rest.RequestHandler;
import hu.dmc.shared.model.Block;
import hu.dmc.shared.model.Element;

import java.util.List;
import java.util.Map;


public class BlockFragment extends Fragment {
    public static final String ARG_SAVED_TEMPLATE_ID = "saved_template_id";
    public static final String ARG_TEMPLATE_ID = "template_id";
    private DataContext dataContext = DataContext.getInstance();
    private Template currentTemplate;
    private SavedTemplate currentSavedTemplate;
    private Block b;

    public BlockFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_block, container, false);
        int savedTemplId = getArguments().getInt(ARG_SAVED_TEMPLATE_ID);
        int templateId = getArguments().getInt(ARG_TEMPLATE_ID);
        for (SavedTemplate st : dataContext.getSavedTemplateList()) {
            if(st.getId().equals(savedTemplId)){
                currentSavedTemplate = st;
                break;
            }
        }
        for (Template t : dataContext.getTemplateList()) {
            if(t.getId().equals(templateId)){
                currentTemplate = t;
                break;
            }
        }
        Gson gson = new Gson();

        b = new Block();
        String jsonString = currentTemplate.getJsonString();
        b.setElementList((List<Element>)gson.fromJson(jsonString, new TypeToken<List<Element>>() {}.getType()));
        b.setDataMap((Map<String, String>)gson.fromJson(currentSavedTemplate.getData(), new TypeToken<Map<String, String>>() {}.getType()));

        ScrollView scrollView = (ScrollView) rootView.findViewById(R.id.saved_frameLayout);
        Button sendButton = (Button) rootView.findViewById(R.id.sendSavedButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                String dataMapJson = gson.toJson(b.getDataMap());
                currentSavedTemplate.setData(dataMapJson);
                String savedTemplate = gson.toJson(currentSavedTemplate);
                RequestHandler.getInstance().sendTemplate(savedTemplate);
            }
        });
        if(!currentTemplate.isVersioned()){
            LinearLayout linearLayout = (LinearLayout)rootView.findViewById(R.id.saved_linearLayout);
            linearLayout.removeView(sendButton);
        }

        GridLayout gridLayout = BlockUtil.getLayout(b, !currentTemplate.isVersioned(), rootView.getContext());
        scrollView.addView(gridLayout);

        getActivity().setTitle(currentSavedTemplate.getName());

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(currentTemplate.isVersioned()) {
            Gson gson = new Gson();
            String dataMapJson = gson.toJson(b.getDataMap());
            currentSavedTemplate.setData(dataMapJson);
            for (int i = 0; i < dataContext.getSavedTemplateList().size(); i++) {
                if(dataContext.getSavedTemplateList().get(i).getId().equals(currentSavedTemplate.getId())){
                    dataContext.getSavedTemplateList().set(i,currentSavedTemplate);
                    break;
                }
            }
        }
    }
}
