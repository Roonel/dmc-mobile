package hu.dmc.mobile.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import dmc.org.mobile.R;
import hu.dmc.mobile.model.DataContext;
import hu.dmc.mobile.model.Message;
import hu.dmc.mobile.model.SavedTemplate;
import hu.dmc.mobile.model.Template;
import hu.dmc.mobile.view.util.MessageAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment that appears in the "content_frame", shows the list of templates available
 */
public class SavedTemplatesFragment extends Fragment implements UpdatableFragment {
    public static final String ARG_TEMPLATE_ID = "template_id";
    private DataContext dataContext = DataContext.getInstance();
    private int templateId;
    private List<String> savedTemplateNameList;
    private ListView sTemplateListView;
    private View rootView;

    public SavedTemplatesFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_saved_temps, container, false);
        templateId = getArguments().getInt(ARG_TEMPLATE_ID);
        sTemplateListView = (ListView) rootView.findViewById(R.id.saved_temp_list);

        sTemplateListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment fragment = new BlockFragment();
                Bundle args = new Bundle();
                int savedTemplateId = 0;
                for (SavedTemplate savedTemplate : dataContext.getSavedTemplateList()) {
                    if (savedTemplate.getTemplateId().equals(templateId) &&
                            savedTemplate.getName().equals(savedTemplateNameList.get(position))) {
                        savedTemplateId = savedTemplate.getId();
                        break;
                    }
                }
                args.putInt(BlockFragment.ARG_SAVED_TEMPLATE_ID, savedTemplateId);
                args.putInt(BlockFragment.ARG_TEMPLATE_ID, templateId);
                fragment.setArguments(args);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            }
        });

        for (Template template : dataContext.getTemplateList()) {
            if (template.getId().equals(templateId)) {
                getActivity().setTitle(template.getName());
                break;
            }
        }

        refreshData();
        return rootView;
    }

    @Override
    public void refreshData() {
        savedTemplateNameList = new ArrayList<>();
        for (SavedTemplate savedTemplate : dataContext.getSavedTemplateList()) {
            if (savedTemplate.getTemplateId().equals(templateId)) {
                savedTemplateNameList.add(savedTemplate.getName());
            }
        }
        sTemplateListView.setAdapter(new ArrayAdapter<>(rootView.getContext(), R.layout.templates_list_item, savedTemplateNameList));
    }
}