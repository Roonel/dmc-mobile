package hu.dmc.mobile.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import dmc.org.mobile.R;
import hu.dmc.mobile.model.DataContext;
import hu.dmc.mobile.model.Message;
import hu.dmc.mobile.view.util.MessageAdapter;

import java.util.ArrayList;

public class MessageListFragment extends Fragment implements UpdatableFragment {
    private DataContext dataContext = DataContext.getInstance();
    private ListView msgListView;
    private View rootView;
    public MessageListFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_message_list, container, false);

        msgListView = (ListView) rootView.findViewById(R.id.msg_list);
        msgListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment fragment = new MessageFragment();
                Bundle args = new Bundle();
                args.putInt(MessageFragment.ARG_MSG_ID, dataContext.getMessageList().get(position).getId());
                fragment.setArguments(args);

                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            }
        });
        refreshData();
        return rootView;
    }

    @Override
    public void refreshData() {
        msgListView.setAdapter(new MessageAdapter(rootView.getContext(), R.layout.messages_list_item,
                dataContext.getMessageList() == null ? new ArrayList<Message>() : dataContext.getMessageList()));
    }
}