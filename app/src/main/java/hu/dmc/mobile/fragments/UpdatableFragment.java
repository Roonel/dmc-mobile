package hu.dmc.mobile.fragments;

/**
 * When data is arriving from the server application and the current active fragment is updatable, it refreshes it's content.
 */
public interface UpdatableFragment {
    void refreshData();
}
