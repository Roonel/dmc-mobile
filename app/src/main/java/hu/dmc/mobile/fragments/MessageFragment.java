package hu.dmc.mobile.fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import dmc.org.mobile.R;
import hu.dmc.mobile.model.DataContext;
import hu.dmc.mobile.model.Message;

import java.io.ByteArrayInputStream;


public class MessageFragment extends Fragment {
    public static final String ARG_MSG_ID = "msg_id";
    private DataContext dataContext = DataContext.getInstance();
    private Message currentMesage;

    public MessageFragment() {
        // Empty constructor required for fragment subclasses
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_message, container, false);
        int msgId = getArguments().getInt(ARG_MSG_ID);
        for (Message m : dataContext.getMessageList()) {
            if(m.getId().equals(msgId)){
                currentMesage = m;
                break;
            }
        }

        EditText body = (EditText) rootView.findViewById(R.id.fragment_msg_body);
        TextView title = (TextView) rootView.findViewById(R.id.fragment_msg_title);

        body.setText(currentMesage.getMessage());
        title.setText(currentMesage.getTitle());

        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.fragment_msg_layout);
        if (currentMesage.getImgData() != null) {
            byte[] imgBytes = Base64.decode(currentMesage.getImgData(), Base64.DEFAULT);
            ImageView imageView = new ImageView(rootView.getContext());
            Bitmap bm = BitmapFactory.decodeByteArray(imgBytes, 0, imgBytes.length);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            imageView.setAdjustViewBounds(true);
            imageView.setImageBitmap(bm);

            layout.addView(imageView);
        }

        return rootView;
    }
}