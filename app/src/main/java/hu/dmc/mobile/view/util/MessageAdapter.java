package hu.dmc.mobile.view.util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import dmc.org.mobile.R;
import hu.dmc.mobile.model.Message;

import java.util.List;

public class MessageAdapter extends ArrayAdapter<Message> {
    private Context mContext;
    private int layoutResourceId;
    private List<Message> data = null;


    public MessageAdapter(Context mContext, int layoutResourceId, List<Message> data) {
        super(mContext, layoutResourceId, data);

        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        Message m = data.get(position);

        TextView title = (TextView) convertView.findViewById(R.id.msg_adapter_title);
        title.setText(m.getTitle());

        TextView sent = (TextView) convertView.findViewById(R.id.msg_adapter_sent);
        sent.setText(m.getSent().toString());

        return convertView;

    }
}
