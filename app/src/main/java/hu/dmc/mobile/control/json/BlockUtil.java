package hu.dmc.mobile.control.json;

import android.content.Context;
import android.text.InputType;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TextView;
import hu.dmc.shared.model.Block;
import hu.dmc.shared.model.Element;

public class BlockUtil {

    public static GridLayout getLayout(Block mb, boolean readOnly, Context context) {
        int maxColumn = 0;
        int maxRow = 0;
        for (Element element : mb.getElementList()) {
            int currRow = element.getYPosition() + element.getHeight();
            if (currRow > maxRow) {
                maxRow = currRow;
            }
            int currCol = element.getXPosition() + element.getWidth();
            if (element.getXPosition() > maxColumn) {
                maxColumn = currCol;
            }
        }

        GridLayout gl = new GridLayout(context);
        gl.setRowCount(maxRow + 1);
        gl.setColumnCount(maxColumn + 1);

        for (Element element : mb.getElementList()) {

            switch (element.getElementType()) {
                case LABEL: {
                    TextView tv = new TextView(context);
                    tv.setText(element.getText());
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
                    layoutParams.rowSpec = GridLayout.spec(element.getYPosition(), element.getHeight());
                    layoutParams.columnSpec = GridLayout.spec(element.getXPosition(), element.getWidth());
                    tv.setLayoutParams(layoutParams);
                    gl.addView(tv);
                    break;
                }
                case TEXT: {
                    EditText et = new EditText(context);
                    String value = mb.getDataMap().get(element.getId());
                    et.setText(value == null || "".equals(value) ? "" : value);
                    et.setHint(element.getText());
                    et.setFocusable(!readOnly && element.getEditable());
                    if (!readOnly && element.getEditable()) {
                        et.addTextChangedListener(new BlockWatcher(mb, element.getId()));
                    }
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
                    layoutParams.rowSpec = GridLayout.spec(element.getYPosition(), element.getHeight());
                    layoutParams.columnSpec = GridLayout.spec(element.getXPosition(), element.getWidth());
                    et.setLayoutParams(layoutParams);
                    gl.addView(et);
                    break;
                }
                case AREA: {
                    EditText et = getArea(context);
                    String value = mb.getDataMap().get(element.getId());
                    et.setText(value == null || "".equals(value) ? "" : value);
                    et.setHint(element.getText());
                    et.setFocusable(!readOnly && element.getEditable());
                    if (!readOnly && element.getEditable()) {
                        et.addTextChangedListener(new BlockWatcher(mb, element.getId()));
                    }
                    GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
                    layoutParams.rowSpec = GridLayout.spec(element.getYPosition(), element.getHeight());
                    layoutParams.columnSpec = GridLayout.spec(element.getXPosition(), element.getWidth());
                    layoutParams.setGravity(Gravity.FILL_HORIZONTAL);
                    et.setLayoutParams(layoutParams);
                    gl.addView(et);
                    break;
                }
            }
        }

        return gl;
    }

    private static EditText getArea(Context context) {
        EditText texInput = new EditText(context);
        texInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE);
        texInput.setLines(5);
        texInput.setVerticalScrollBarEnabled(true);
        return texInput;
    }
}


