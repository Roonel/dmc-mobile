package hu.dmc.mobile.control.json;

import android.text.Editable;
import android.text.TextWatcher;
import hu.dmc.shared.model.Block;

public class BlockWatcher implements TextWatcher {
    private Block block;
    private String id;

    public BlockWatcher(Block block, String id) {
        this.block = block;
        this.id = id;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        block.getDataMap().put(id,s.toString());
    }
}
