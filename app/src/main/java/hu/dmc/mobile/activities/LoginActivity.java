package hu.dmc.mobile.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import cz.msebera.android.httpclient.Header;
import dmc.org.mobile.R;
import hu.dmc.mobile.model.DataContext;
import hu.dmc.mobile.model.User;
import hu.dmc.mobile.util.rest.HttpUtils;
import hu.dmc.mobile.util.rest.RequestHandler;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends ActionBarActivity {

    private static final String LOGTAG = "MAIN_ACTIVITY";
    public static final String PREFS_NAME = "DmcPrefs";
    private DataContext dataContext = DataContext.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        String lastIp = settings.getString("lastIp", "");
        String userName = settings.getString("userName", "");

        final EditText userText = (EditText) findViewById(R.id.userNameEditText);
        userText.setText(userName);

        final EditText ipText = (EditText) findViewById(R.id.ipAddressEditText);
        ipText.setText(lastIp);


        final EditText passwordText = (EditText) findViewById(R.id.passwordEditText);

        userText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                if(ipText.getText().toString().length() > 0 && passwordText.getText().toString().length() > 0) {
                    findViewById(R.id.loginBtn).setEnabled(true);
                }
            }
        });
        ipText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                if(userText.getText().toString().length() > 0 && passwordText.getText().toString().length() > 0) {
                    findViewById(R.id.loginBtn).setEnabled(true);
                }
            }
        });
        passwordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                if(userText.getText().toString().length() > 0 && ipText.getText().toString().length() > 0) {
                    findViewById(R.id.loginBtn).setEnabled(true);
                }
            }
        });

        dataContext.setContext(this);
    }

    public void enterApplication() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    public void connect(View view) {
        final EditText ipText = (EditText) findViewById(R.id.ipAddressEditText);
        final EditText userText = (EditText) findViewById(R.id.userNameEditText);
        final EditText passwordText = (EditText) findViewById(R.id.passwordEditText);

        if(ipText.getText().toString() == null || "".equals(ipText.getText().toString())){
            Toast.makeText(dataContext.getContext(), getString(R.string.reqIp), Toast.LENGTH_SHORT).show();
            return;
        }

        if(userText.getText().toString() == null || "".equals(userText.getText().toString())){
            Toast.makeText(dataContext.getContext(), getString(R.string.reqUser), Toast.LENGTH_SHORT).show();
            return;
        }

        if(passwordText.getText().toString() == null || "".equals(passwordText.getText().toString())){
            Toast.makeText(dataContext.getContext(), getString(R.string.reqPass), Toast.LENGTH_SHORT).show();
            return;
        }


        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.loading));
        progress.setMessage(getString(R.string.connectingTo) + ipText.getText().toString());
        progress.setCancelable(false);
        progress.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (dataContext.getSuccessfulRestCall() != null && dataContext.getSuccessfulRestCall()) {
                    enterApplication();
                }
            }
        });
        progress.show();
        HttpUtils.setIp(ipText.getText().toString());
        RequestParams rp = new RequestParams();
        rp.add("user",userText.getText().toString());
        //passwords are stored in sha256 hashed format
        //TODO ez valamiért nem müködik
        //rp.add("password", DigestUtils.sha256Hex(passwordText.getText().toString()));
        rp.add("password", passwordText.getText().toString());
        HttpUtils.get("/", rp, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if("Unauthorized".equals(throwable.getMessage())) {
                    Toast.makeText(dataContext.getContext(), getString(R.string.wrongPass), Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(dataContext.getContext(), getString(R.string.connectionError), Toast.LENGTH_SHORT).show();
                progress.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    String userData = response.getString("data");
                    User entity = gson.fromJson(userData,new TypeToken<User>() {}.getType());
                    if (entity!= null) {
                        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("lastIp", ipText.getText().toString());
                        editor.putString("userName", userText.getText().toString());
                        editor.apply();
                        dataContext.setUser(entity);
                        progress.setMessage(getString(R.string.downloadingData));
                        RequestHandler.getInstance().getTemplateData(progress);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
