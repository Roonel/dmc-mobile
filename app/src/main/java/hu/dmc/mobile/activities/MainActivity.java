package hu.dmc.mobile.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import dmc.org.mobile.R;
import hu.dmc.mobile.fragments.BlockFragment;
import hu.dmc.mobile.fragments.MessageFragment;
import hu.dmc.mobile.fragments.MessageListFragment;
import hu.dmc.mobile.fragments.SavedTemplatesFragment;
import hu.dmc.mobile.model.DataContext;
import hu.dmc.mobile.model.Template;
import hu.dmc.mobile.util.rest.TimerManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ActionBarActivity {
    private DataContext dataContext = DataContext.getInstance();
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private List<String> templateNames;

    private int currentPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        templateNames = new ArrayList<>();
        templateNames.add(getString(R.string.messages));
        for (Template template : dataContext.getTemplateList()) {
            templateNames.add(template.getName());
        }
        mDrawerLayout.setDrawerShadow(R.mipmap.drawer_shadow, GravityCompat.START);
        mDrawerList.setAdapter(new ArrayAdapter<>(this, R.layout.drawer_list_item, templateNames));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
            }

            @Override
            public boolean onOptionsItemSelected(android.view.MenuItem item) {
                return mDrawerToggle.onOptionsItemSelected(item);
            }
        };
        mDrawerToggle.setHomeAsUpIndicator(R.mipmap.ic_drawer);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }
        dataContext.setContext(this);
        dataContext.setFragmentManager(getFragmentManager());
        TimerManager.startDataTimer();
    }

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        Fragment fragment;
        if (position == 0) {
            fragment = new MessageListFragment();
        } else {
            fragment = new SavedTemplatesFragment();
            Bundle args = new Bundle();
            args.putInt(SavedTemplatesFragment.ARG_TEMPLATE_ID, dataContext.getTemplateList().get(position-1).getId());
            fragment.setArguments(args);
        }

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        mDrawerList.setItemChecked(position, true);
        setTitle(templateNames.get(position));
        mDrawerLayout.closeDrawer(mDrawerList);
        currentPosition = position;
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            mDrawerLayout.openDrawer(mDrawerList);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        Fragment f = getFragmentManager().findFragmentById(R.id.content_frame);
        if (f instanceof BlockFragment) {
            selectItem(currentPosition);
        } else if (f instanceof MessageFragment) {
            selectItem(0);
        } else {
            if (currentPosition == 0) {
                TimerManager.stopMessageTimer();
                super.onBackPressed();
            }else{
                selectItem(0);
            }
        }
    }
}