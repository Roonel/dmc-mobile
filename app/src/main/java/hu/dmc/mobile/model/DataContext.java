package hu.dmc.mobile.model;

import android.app.FragmentManager;
import android.content.Context;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

public @Data class DataContext {

    private static DataContext instance;
    private DataContext(){
    }

    private User user;
    private List<Template> templateList;
    private List<SavedTemplate> savedTemplateList;
    private List<Message> messageList;
    private Boolean successfulRestCall;
    private Context context;
    private FragmentManager fragmentManager;
    private Timestamp lastReceived;

    public static DataContext getInstance(){
        if(instance == null){
            instance = new DataContext();
        }
        return instance;
    }
}
