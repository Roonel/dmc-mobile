package hu.dmc.mobile.model;

import lombok.Data;

public @Data class Template {
    private Integer id;
    private String name;
    private String jsonString;
    private String moduleId;
    private boolean clientTemplate;
    private boolean versioned;
}
