package hu.dmc.mobile.model;

import lombok.Data;

import java.sql.Timestamp;

public @Data class Message {
    private Integer id;
    private String message;
    private String title;
    private String imgData;
    private Timestamp sent;
    private Integer recipient;
}
