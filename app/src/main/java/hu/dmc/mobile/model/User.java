package hu.dmc.mobile.model;

import lombok.Data;

import java.sql.Timestamp;

public @Data class User {
    private int id;
    private String name;
    private String password;
    private Timestamp lastOnline;
}
