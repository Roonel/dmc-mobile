package hu.dmc.mobile.model;

import lombok.Data;

import java.sql.Timestamp;

public @Data class SavedTemplate {
    private Integer id;
    private String name;
    private Integer templateId;
    private String data;
    private Integer ownedBy;
    private Boolean original;
    private Timestamp version;
    private Timestamp updated;
    private Integer modifiedBy;
}
