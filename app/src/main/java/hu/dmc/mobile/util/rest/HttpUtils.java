package hu.dmc.mobile.util.rest;

import com.loopj.android.http.*;

public class HttpUtils {
    private static String BASE_URL;

    private static AsyncHttpClient client = new AsyncHttpClient(8080);
    private static SyncHttpClient syncClient = new SyncHttpClient(8080);

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.setMaxRetriesAndTimeout(3,1000);
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void getSync(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        syncClient.setMaxRetriesAndTimeout(3,1000);
        syncClient.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void getByUrl(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(url, params, responseHandler);
    }

    public static void postByUrl(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(url, params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    public static void setIp(String ip){
         BASE_URL = "http://"+ip;
     }
}