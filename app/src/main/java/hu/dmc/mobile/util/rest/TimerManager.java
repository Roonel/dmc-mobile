package hu.dmc.mobile.util.rest;

import java.util.Timer;
import java.util.TimerTask;

public class TimerManager {
    private static Timer dataTimer;

    public static void startDataTimer(){
        dataTimer = new Timer("msg");
        dataTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                RequestHandler.getInstance().gatherData();
            }
        },0,5000);
    }

    public static void stopMessageTimer(){
        if(dataTimer != null){
            dataTimer.cancel();
            dataTimer = null;
        }
    }
}
