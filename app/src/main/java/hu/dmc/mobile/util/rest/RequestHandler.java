package hu.dmc.mobile.util.rest;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import cz.msebera.android.httpclient.Header;
import dmc.org.mobile.R;
import hu.dmc.mobile.fragments.MessageListFragment;
import hu.dmc.mobile.fragments.UpdatableFragment;
import hu.dmc.mobile.model.DataContext;
import hu.dmc.mobile.model.Message;
import hu.dmc.mobile.model.SavedTemplate;
import hu.dmc.shared.rest.RefreshData;
import hu.dmc.shared.rest.TemplateData;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class RequestHandler {

    private static RequestHandler instance;
    private static final String LOGTAG = "REQUEST_HANDLER";
    private DataContext dataContext = DataContext.getInstance();

    private RequestHandler() {
    }

    public static RequestHandler getInstance() {
        if (instance == null) {
            instance = new RequestHandler();
        }
        return instance;
    }

    public void getTemplateData(final ProgressDialog progress) {
        dataContext.setSuccessfulRestCall(null);
        RequestParams rp = new RequestParams();
        rp.put("user",dataContext.getUser().getId());
        HttpUtils.get("/data/templates", rp, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.d(LOGTAG, "Error while gathering Template data");
                error();
            }

            private void error() {
                Toast.makeText(progress.getContext(), "Error while downloading data!", Toast.LENGTH_SHORT).show();
                dataContext.setSuccessfulRestCall(false);
                progress.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    if(statusCode == HttpStatus.SC_NO_CONTENT){
                        Toast.makeText(progress.getContext(), "No available data found", Toast.LENGTH_SHORT).show();
                        dataContext.setSuccessfulRestCall(false);
                        progress.dismiss();
                        return;
                    }

                    Gson gson = new Gson();
                    String data = response.get("data").toString();
                    TemplateData td = gson.fromJson(data, new TypeToken<TemplateData>() {
                    }.getType());
                    dataContext.setTemplateList(td.getTemplateList());
                    dataContext.setSavedTemplateList(td.getSavedTemplateList());
                    dataContext.setSuccessfulRestCall(true);
                    progress.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    error();
                }
            }
        });
    }

    public void gatherData(){
        RequestParams rp = new RequestParams();
        rp.put("user",dataContext.getUser().getId());
        if(dataContext.getLastReceived() != null){
            rp.put("after",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(dataContext.getLastReceived()));
        }

        HttpUtils.getSync("/data/refresh", rp, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Log.d(LOGTAG, "Error while gathering data");
                error();
            }

            private void error() {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                            Toast.makeText(dataContext.getContext(), dataContext.getContext().getString(R.string.msgError), Toast.LENGTH_SHORT).show();
                    }
                }, 1000 );
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    RefreshData refreshData =  gson.fromJson(response.get("data").toString(),
                            new TypeToken<RefreshData>() {}.getType());
                    List<Message> msgList = refreshData.getMessages();

                    final boolean hasMsg = msgList != null && !msgList.isEmpty();
                    Timestamp lastReceived = dataContext.getLastReceived();
                    if(hasMsg) {
                        if (dataContext.getMessageList() != null) {
                            dataContext.getMessageList().addAll(msgList);
                        } else {
                            dataContext.setMessageList(msgList);
                        }
                        for (Message message : msgList) {
                            if(lastReceived==null) {
                                lastReceived = message.getSent();
                            }
                            if(message.getSent().after(lastReceived)){
                                lastReceived = message.getSent();
                            }
                        }
                        Log.d(LOGTAG, "data has arrived: " + msgList.toString());
                    }

                    List<SavedTemplate> templates = refreshData.getSavedTemplateList();
                    if(templates != null && !templates.isEmpty()){
                        for (SavedTemplate templ : templates) {
                            List<SavedTemplate> dataContextList = dataContext.getSavedTemplateList();
                            for (int i = 0; i < dataContextList.size(); i++) {
                                if(dataContextList.get(i).getId().equals(templ.getId())){
                                    dataContextList.set(i,templ);
                                    if(lastReceived == null) {
                                        lastReceived = templ.getUpdated();
                                    }
                                    if(templ.getVersion().after(lastReceived)){
                                        lastReceived = templ.getUpdated();
                                    }
                                    break;
                                }
                            }
                        }
                        Log.d(LOGTAG, "data has arrived4: " + templates.toString());
                    }

                    dataContext.setLastReceived(lastReceived);

                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Fragment f = dataContext.getFragmentManager().findFragmentById(R.id.content_frame);
                            if(f instanceof UpdatableFragment){
                                ((UpdatableFragment)f).refreshData();
                            }
                            if (!(f instanceof MessageListFragment) && hasMsg){
                                Toast.makeText(dataContext.getContext(), dataContext.getContext().getString(R.string.newMsg), Toast.LENGTH_LONG).show();
                            }
                        }
                    }, 1000 );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void sendTemplate(String savedTemplate) {
        RequestParams rp = new RequestParams();
        rp.put("user",dataContext.getUser().getId());
        rp.put("templateData",savedTemplate);
        HttpUtils.get("/data/saveTemplate", rp, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                if (statusCode == HttpStatus.SC_ACCEPTED) {
                    success();
                } else{
                    error();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                if (statusCode == HttpStatus.SC_ACCEPTED) {
                    success();
                } else{
                    error();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (statusCode == HttpStatus.SC_ACCEPTED) {
                    success();
                } else{
                    error();
                }
            }

            private void error() {
                Log.d(LOGTAG, "Error while sending data");
                Toast.makeText(dataContext.getContext(), dataContext.getContext().getString(R.string.successfulSave), Toast.LENGTH_SHORT).show();
            }
            private void success() {
                Toast.makeText(dataContext.getContext(), dataContext.getContext().getString(R.string.errorWhileSave), Toast.LENGTH_SHORT).show();
                dataContext.setSuccessfulRestCall(false);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if(statusCode == HttpStatus.SC_ACCEPTED){
                    success();
                }
            }
        });
    }

}