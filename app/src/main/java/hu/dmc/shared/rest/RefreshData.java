package hu.dmc.shared.rest;

import hu.dmc.mobile.model.Message;
import hu.dmc.mobile.model.SavedTemplate;
import lombok.Data;

import java.util.List;

public @Data class RefreshData {
    private List<Message> messages;
    private List<SavedTemplate> savedTemplateList;
}
