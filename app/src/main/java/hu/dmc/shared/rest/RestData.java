package hu.dmc.shared.rest;

/**
 * Simple object for handling http responses
 */
public class RestData {
    /**
     * The actual data from the response
     */
    private Object data;
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
