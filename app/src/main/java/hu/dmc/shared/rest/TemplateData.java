package hu.dmc.shared.rest;

import hu.dmc.mobile.model.SavedTemplate;
import hu.dmc.mobile.model.Template;
import lombok.Data;

import java.util.List;

public @Data class TemplateData {
    private List<Template> templateList;
    private List<SavedTemplate> savedTemplateList;
}
