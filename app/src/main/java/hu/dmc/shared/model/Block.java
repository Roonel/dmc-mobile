package hu.dmc.shared.model;

import lombok.Data;

import java.util.List;
import java.util.Map;

public @Data class Block {
    private List<Element> elementList;
    private Map<String, String> dataMap;
}
