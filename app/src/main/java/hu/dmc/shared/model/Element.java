package hu.dmc.shared.model;

import lombok.Data;

import java.util.List;

public @Data class Element {
    private String id;
    private ElementType elementType;
    private String text;
    private Boolean editable;
    private List<String> style;

    private Integer xPosition;
    private Integer yPosition;
    private Integer width;
    private Integer height;
}
