package hu.dmc.shared.model;

public enum ElementType {
    LABEL, TEXT, AREA
}
